import {useState, useEffect} from 'react';
import Link from 'next/link';
import moment from 'moment';
import Fuse from 'fuse.js';
import Swal from 'sweetalert2';
import Head from 'next/head';



/*Bootstrap*/
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Button from 'react-bootstrap/Button';
import Alert from 'react-bootstrap/Alert';
import InputGroup from 'react-bootstrap/InputGroup';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';

export default function Records () {
	
	const [data, setData] = useState([])
	const [total, setTotal] = useState([])
	const [search, setSearch] = useState("")
	const [filter, setFilter] = useState("All")
	const [click, setClick] = useState(false)

	useEffect(() => {

		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}api/users/user-details`, {
			method : "Get",
			headers : {

				"Authorization" : `Bearer ${localStorage.getItem("token")}`
			},

		})
		.then(res => res.json())
		.then(data => {

			if(filter === "All"){
				setData(data.expenses)
			}else {

			setData(data.expenses.filter(word => word.type === filter))
			}


			let acc = 0
			data.expenses.forEach(element => {
				if(element.type == "Income") {
					acc = acc + element.amount
				} else {
					if(element.type == "Expense") {
						acc = acc - element.amount
					}
				}
			})

		setTotal(acc)
		})

	},[filter, click])



	const fuse = new Fuse(data, {
	  keys: [
	    'name',
	    'type',
	    'description'
	  ]
	});
	const results = fuse.search(search)
	const itemResults = search ? results.map(character => character.item) : data;


	const deleteRecord = (recordId) => {

		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}api/users/${recordId}`,{
			method : "DELETE",

			headers : {
				"Authorization" : `Bearer ${localStorage.getItem("token")}`,
				"Content-Type" : "application/json"
			}

		})
		.then(res => res.json())
		.then(data => {
			if(data){
				setClick(false)
				Swal.fire({
				  icon : "success",
				  text: 'Succesfully Deleted'
				})
			}

		})
	}
	

	return(


			<React.Fragment>
			<Head>
			  <title>Records</title>
			 <meta charset="UTF-8" />
			 <meta name="description" content="Budget Tracker"/>
			 <meta name="keywords" content="HTML, CSS, JavaScript"/>
			 <meta name="viewport" content="initial-scale=1.0, width=device-width" />
			</Head>
			<h1>Records</h1>

			<InputGroup className="mb-3">
			<Link href="/add">
				<Button>Add</Button>
			</Link>
			  <Form.Control type="text" value={search} onChange={e => setSearch(e.target.value)}/>
			  	<Form.Control as="select" placreholder="test" onChange={e => setFilter(e.target.value)}>
			  		<option value="All">All</option>
			  		<option value="Income">Income</option>
			  		<option value="Expense">Expense</option>
			  	</Form.Control>
			</InputGroup>
			<h3>Current Balance : {total} PHP</h3>
				<Row className="my-5 row-cols-sm-2 row-cols-md-3 row-cols-lg-4 row-cols-xl-5">

				{
					itemResults.map(record => {
						return (
								<Col key={record._id} sm= "4" md="2">
								<Card className="mt-1">
								  <Card.Body>
								    <Card.Title>{record.description}</Card.Title>
								    <Card.Text>
								    	{record.type}({record.name})
								    </Card.Text>
								    <Card.Text>
								    	{record.type == "Expense" ? <Alert variant="danger">-{record.amount}</Alert> : <Alert variant="success">+{record.amount}</Alert>}
								    </Card.Text>
								    <Card.Text>
								    	Total Balance: {record.type == "Expense" ? `${record.total - record.amount} PHP` : `${record.total + record.amount} PHP`}
								    </Card.Text>
								    <Card.Text>
								    	{moment(record.date).format("MMMM DD, YYYY")}
								    </Card.Text>
								  </Card.Body>
								    <Button variant="danger" onClick={() => {deleteRecord(record._id); setClick(true);}}>Delete</Button>
								</Card>
								</Col>
							)
					})
				}

				</Row>
				</React.Fragment>


		)
}