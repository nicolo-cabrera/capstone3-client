import {useState, useEffect} from 'react';
import Router from 'next/router';
import Swal from 'sweetalert2';
import Head from 'next/head';

/*Bootstrap*/
import Form from  'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';



export default function New () {

	const [type, setType] = useState("Income")
	const [name, setName] = useState("")
	const [isActive, setIsActive] = useState(false)

	useEffect(() => {
		if(name !== "") {
			return setIsActive(true)
		} else {
			return setIsActive(false)
		}
	}, [name])

	const addCategory = (e) => {
		e.preventDefault()
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}api/users/add-category`, {
			method : "POST",
			headers : {
				"Content-Type" : "application/json",
				"Authorization" : `Bearer ${localStorage.getItem("token")}`
			},

			body : JSON.stringify({
				category : name,
				type : type
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data) {
				Swal.fire({
				  icon : "success",
				  text: 'Category Added!'
				})
				Router.push("/categories")
			} else {
				Swal.fire({
				  icon : "error",
				  text: 'Something went wrong. Try again.'
				})
			}
		})
	}

	return (
			<React.Fragment>
			<Head>
			  <title>Add Category</title>
			  <meta charset="UTF-8" />
			  <meta name="description" content="Budget Tracker"/>
			  <meta name="keywords" content="HTML, CSS, JavaScript"/>
			  <meta name="viewport" content="initial-scale=1.0, width=device-width" />
			</Head>
			<Form onSubmit={(e) => addCategory(e)}>
				<h1>Add Category</h1>
				<Form.Group controlId="name">
					<Form.Label>Category Name</Form.Label>
					<Form.Control type="text" value={name} onChange={e => setName(e.target.value)}required/>
				</Form.Group>
				<Form.Group controlId="type">
					<Form.Label>Category Type</Form.Label>
					<Form.Control as="select" placeholder="Group" onChange={e => setType(e.target.value)}>
						<option value="Income">Income</option>
						<option value="Expense">Expense</option>
					</Form.Control>
				</Form.Group>
				{
					isActive ?
				<Button type="submit">Submit</Button>
				:
				<Button disabled>Submit</Button>
				}
			</Form>	
			</React.Fragment>


		)
}