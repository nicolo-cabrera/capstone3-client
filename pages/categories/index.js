import {useState, useEffect} from 'react';
import Link from 'next/link';
import Swal from 'sweetalert2';
import Head from 'next/head';

/*Bootstrap*/
import Button from 'react-bootstrap/Button';
import Table from 'react-bootstrap/Table';
import Container from 'react-bootstrap/Container';


export default function Categories () {
	const [categories, setCategories] = useState([])
	const [click, setClick] = useState(false)

	useEffect(() => {

		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}api/users/user-details`, {
			headers : {
				"Authorization" : `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			
			setCategories(data.categories)
		})

	},[click])


	const deleteCategory = (categoryId) => {

		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}api/users/delete/${categoryId}`,{
			method : "DELETE",

			headers : {
				"Authorization" : `Bearer ${localStorage.getItem("token")}`,
				"Content-Type" : "application/json"
			}

		})
		.then(res => res.json())
		.then(data => {
			setClick(false)
			Swal.fire({
			  icon : "success",
			  text: 'Succesfully Deleted'
			})

		})
	}

	return (


			<React.Fragment>
			<Head>
			  <title>Categories</title>
			  <meta charset="UTF-8" />
			  <meta name="description" content="Budget Tracker"/>
			  <meta name="keywords" content="HTML, CSS, JavaScript"/>
			  <meta name="viewport" content="initial-scale=1.0, width=device-width" />
			</Head>
				<h1>Categories</h1>
				<Link href="/categories/new">
					<Button>Add</Button>
				</Link>
				<Table striped bordered hover responsive>
					<thead>
						<tr>
							<th>Category</th>
							<th>Type</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						{
							categories.map(category => {
								return(

										<tr key={category._id}>
											<td>{category.category}</td>
											<td>{category.type}</td>
											<td><Button variant="danger"onClick={() => {deleteCategory(category._id); setClick(true);}}>Delete</Button></td>
										</tr>
									)
							})
						}
					</tbody>	
				</Table>	
			</React.Fragment>

		)
}
