import {useEffect, useState} from 'react';
import Head from 'next/head';

/*Import moment*/
import moment from 'moment';

/*Import bootstrap*/
import {Tabs, Tab} from 'react-bootstrap';

/*Import component*/
import BarChart from '../../components/BarChart';
import LineChart from '../../components/LineChart';

export default function Chart() {

	const [income, setIncome] = useState([])
	const [expense, setExpense] = useState([])
	const [total, setTotal] = useState([])

	useEffect( () => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}api/users/user-details`, {
			headers : {
				"Authorization" : `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
	
			if( data.expenses.length > 0 ) {

				let monthlyIncome = [0,0,0,0,0,0,0,0,0,0,0,0]
				let monthlyExpense = [0,0,0,0,0,0,0,0,0,0,0,0]

				data.expenses.forEach(element => {
					const index = moment(element.date).month()

					if(element.type === "Expense") {
					monthlyExpense[index] = monthlyExpense[index] + (element.amount)

				} else {

					monthlyIncome[index] = monthlyIncome[index] + (element.amount)
				}

				})

				setIncome(monthlyIncome)
				setExpense(monthlyExpense)
				setTotal(data.expenses)

			}
			
		})

	},[])

	return (

			<React.Fragment>
				<Head>
					<title>Insight</title>
					<meta charset="UTF-8" />
					<meta name="description" content="Budget Tracker"/>
					<meta name="keywords" content="HTML, CSS, JavaScript"/>
					<meta name="viewport" content="initial-scale=1.0, width=device-width" />
				</Head>
				<Tabs defaultActiveKey="income" id="monthly Income">
					<Tab eventKey="income" title="Monthly Income">
						<BarChart figuresArray={income} label="Monthly Income"/>
					</Tab>
					<Tab eventKey="expense" title="Monthly Expense">
						<BarChart figuresArray={expense} label="Monthly Expense"/>
					</Tab>
					<Tab eventKey="trend" title="Monthly Trend">
						<LineChart figuresArray={total}/>
					</Tab>
				</Tabs>

			</React.Fragment>

		)

}
