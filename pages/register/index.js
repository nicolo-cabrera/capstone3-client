import {useState, useEffect} from 'react';
import Router from 'next/router';
import Head from 'next/head';
import Swal from 'sweetalert2';

/*Bootstrap*/
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';


export default function Register () {

	const [firstName, setFirstName] = useState("")
	const [lastName, setLastName] = useState("")
	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [password2, setPassword2] = useState("")

	const [isActive, setIsActive] = useState(false)

	useEffect(() => {
		if(
			firstName !== "" 
			&& lastName !== "" 
			&& email !== ""
			&& password !== ""
			&& password2 !== ""
			&& password === password2)
		{
			return setIsActive(true)
		} else {
			return setIsActive(false)
		}

	},[firstName, lastName, email, password, password2])

	const register = (e) => {
		e.preventDefault()

		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}api/users/email-exist`,{
			method : "POST",

			headers : {
				"Content-Type" : "application/json"
			},

			body : JSON.stringify({
				email : email
			})
		})
		.then(res => res.json())
		.then(data =>{

			if(data) {

				Swal.fire({
				  icon : "error",
				  text: 'Email already exist'
				})

			} else {

				fetch(`${process.env.NEXT_PUBLIC_BASE_URL}api/users`, {
					method : "POST",
					headers : {
						"Content-Type" : "application/json"
					},

					body : JSON.stringify({
						firstName : firstName,
						lastName : lastName,
						email : email,
						password : password
					})

				})
				.then(res => res.json())
				.then(data => {
					if(data) {

						Swal.fire({
						  icon : "success",
						  text: 'Account Created!'
						})

						Router.push("/login")
						setFirstName("")
						setLastName("")
						setEmail("")
						setPassword("")
						setPassword2("")
					} else {

						Swal.fire({
						  icon : "error",
						  text: 'Something went wrong. Try again.'
						})
					}
				})
			}
		})
}

	return (
		<React.Fragment>
		<Head>
		  <title>Register</title>
		  <meta charset="UTF-8" />
		  <meta name="description" content="Budget Tracker" />
		  <meta name="keywords" content="HTML, CSS, JavaScript" />
		  <meta name="viewport" content="initial-scale=1.0, width=device-width" />
		</Head>
		<h1>Register</h1>
			<Row>
				<Col>
				<Form onSubmit={e => register(e)}>
				  <Form.Group controlId="firstName">
				    <Form.Label>First Name</Form.Label>
				    <Form.Control type="text" value={firstName} onChange={e => setFirstName(e.target.value)} required/>
				  </Form.Group>
				  <Form.Group controlId="lastName">
				    <Form.Label>Last Name</Form.Label>
				    <Form.Control type="text" value={lastName} onChange={e => setLastName(e.target.value)} required/>
				  </Form.Group>
				  <Form.Group controlId="email">
				    <Form.Label>Email</Form.Label>
				    <Form.Control type="email" value={email} onChange={e => setEmail(e.target.value)} required/>
				  </Form.Group>
				  <Form.Group controlId="password">
				    <Form.Label>Password</Form.Label>
				    <Form.Control type="password" value={password} onChange={e => setPassword(e.target.value)} required/>
				  </Form.Group>
				  <Form.Group controlId="password2">
				    <Form.Label>Confirm Password</Form.Label>
				    <Form.Control type="password" value={password2} onChange={e => setPassword2(e.target.value)} required/>
				  </Form.Group>

				  {
				  	isActive ?
				  	<Button type="submit">Submit</Button>
				  	:
				  	<Button disabled>Submit</Button>
				  }

				</Form>
				</Col>
			</Row>
		</React.Fragment>

		)
}