import {useState, useEffect} from 'react';
import Router from 'next/router';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/globals.css';
/*Bootstrap*/
import Container from 'react-bootstrap/Container';

import NProgress from 'nprogress'; 
import 'nprogress/nprogress.css'; 

/*components*/
import NavBar from '../components/NavBar';


/*UserProvider*/
import {UserProvider} from '../UserContext';


Router.events.on('routeChangeStart', () => NProgress.start()); 
Router.events.on('routeChangeComplete', () => NProgress.done()); 
Router.events.on('routeChangeError', () => NProgress.done());
NProgress.configure({ showSpinner: false });



function MyApp({ Component, pageProps }) {

	const [user, setUser] = useState({
		id : null
	})

	useEffect(() => {

		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}api/users/user-details`, {
			headers : {
				"Authorization" : `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setUser({
				id : data._id,
			})
		})

	},[])

  return (
  		<React.Fragment>
	  		<UserProvider value={{user, setUser}}>
			  	<NavBar />
	  			<Container className="mt-5">
			  	<Component {...pageProps} />
	  			</Container>
			</UserProvider>
  		</React.Fragment>

  	)
}

export default MyApp
