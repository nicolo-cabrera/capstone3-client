import {useState, useContext} from 'react';
import UserContext from '../../UserContext'
import Router from 'next/router';
import Link from 'next/link';
import Swal from 'sweetalert2';
import Head from 'next/head';

/*Bootstrap*/
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

export default function Login() {
  const {setUser} = useContext(UserContext)

  const [email ,setEmail] = useState("")
  const [password, setPassword] = useState("")

  const login = (e) => {

    e.preventDefault()

    fetch(`${process.env.NEXT_PUBLIC_BASE_URL}api/users/login`, {

      method : "POST",
      headers : {
        "Content-Type" : "application/json"
      },
      body : JSON.stringify({
        email : email,
        password : password
      })
    })
    .then(res => res.json())
    .then(data => {
      if(data.access) {
        localStorage.setItem("token", data.access)
        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}api/users/user-details`, {
          method : "GET",
          headers : {
            "Authorization" : `Bearer ${data.access}`
          }
        })
        
        .then(res => res.json())
        .then(data => {
          setUser({
            id : data._id
          })

          Swal.fire({
            icon : "success",
            text: 'Login Success'
          })

          setEmail("")
          setPassword("")
          Router.push("/")

         
        })

      } else {

        Swal.fire({
          icon : "error",
          text: 'Account does not exist'
        })

      }
    })

  }



  return (
        <React.Fragment>
          <Head>
            <title>Login</title>
            <meta charSet="UTF-8" />
            <meta name="description" content="Budget Tracker"/>
            <meta name="keywords" content="HTML, CSS, JavaScript"/>
            <meta name="viewport" content="initial-scale=1.0, width=device-width" />
          </Head>
          <Row>
            <Col md={6}>
              <h1>Budget Tracker</h1>
            </Col>
            <Col md={6}>
              <Form onSubmit={e => login(e)}>
              <h1>Login</h1>
                <Form.Group controlId="email">
                  <Form.Label>Email</Form.Label>
                  <Form.Control type="email" value={email} onChange={e => setEmail(e.target.value)} required/>
                </Form.Group>
                <Form.Group controlId="password">
                  <Form.Label>Password</Form.Label>
                  <Form.Control type="password" value={password} onChange={e => setPassword(e.target.value)} required/>
                </Form.Group>
                <Button type="submit">Submit</Button>
                <Form.Text>
                  <Link href="/forgot">
                    <a>Forgot Password?</a>
                  </Link>
                </Form.Text>
              </Form>
            </Col>
          </Row>
        </React.Fragment>



    )
}