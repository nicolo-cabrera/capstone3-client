import Head from 'next/head';
import {useState} from 'react'

/*Bootstrap*/
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Image from 'react-bootstrap/Image';

export default function Home() {


  return (


    <React.Fragment>
    <Head>
      <title>Budget Tracker</title>
      <meta charset="UTF-8" />
      <meta name="description" content="Budget Tracker"/>
      <meta name="keywords" content="HTML, CSS, JavaScript"/>
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>
    <Row className="mt-5">
    	<Col md={6}>
    		<Image src="/coinss.png" alt="coins "fluid />
    	</Col>
    	<Col md={6}>
    		<h1>Budget Tracker</h1>
    	</Col>
    </Row>
    </React.Fragment>

  )
}
