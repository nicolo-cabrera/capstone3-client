import {useState, useEffect} from 'react';
import Router from 'next/router';
import Swal from 'sweetalert2';
import Head from 'next/head';

/*Bootstrap*/
import Form from  'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';




export default function Add () {
	const [data, setData] = useState([])
	const [type, setType] = useState("Income")
	const [name, setName] = useState("")
	const [amount, setAmount] = useState(0)
	const [total, setTotal] = useState(0)
	const [description, setDescription] = useState("")
	const [isActive, setIsActive] = useState(false)

	useEffect(() => {

		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}api/users/user-details`, {
			method : "Get",
			headers : {

				"Authorization" : `Bearer ${localStorage.getItem("token")}`
			},

		})
		.then(res => res.json())
		.then(data => {
			
			if(data.categories.length < 1){
				Router.push("/categories/new")
				Swal.fire({
				  icon : "warning",
				  text: 'You need to add category first!'
				})

			}else {
				setName(data.categories.[0].category)
			}

			
			setData(data.categories)

				let acc = 0
				data.expenses.forEach(element => {
					if(element.type == "Income") {
						acc = acc + element.amount
					} else {
						if(element.type == "Expense") {
							acc = acc - element.amount
						}
					}
				})

			setTotal(acc)

		})

	},[])

	useEffect(() => {
		if(name !== ""){

			return setIsActive(true)
		}else {

			return setIsActive(false)
		}
	},[name])

	const addRecords = (e) => {
		e.preventDefault()
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}api/users/add-record`, {
			method : "POST",
			headers : {
				"Content-Type" : "application/json",
				"Authorization" : `Bearer ${localStorage.getItem("token")}`
			},

			body : JSON.stringify({
				name : name,
				type : type,
				amount : amount,
				description : description,
				total : total
				
			})


		})
		.then(res => res.json())
		.then(data => {
			if(data) {
				Swal.fire({
				  icon : "success",
				  text: 'Record Added!'
				})
				Router.push("/records")
			} else {
				Swal.fire({
				  icon : "error",
				  text: 'Something went wrong. Try again.'
				})
			}
		})
	}

	return (

		<React.Fragment>
		<Head>
		  <title>Add Record</title>
		  <meta charset="UTF-8" />
		  <meta name="description" content="Budget Tracker"/>
		  <meta name="keywords" content="HTML, CSS, JavaScript"/>
		  <meta name="viewport" content="initial-scale=1.0, width=device-width" />
		</Head>
		<h1>Add Record</h1>
			<Form onSubmit={e => addRecords(e)}>
			<Form.Group controlId="name">
				<Form.Label>Category Name</Form.Label>
				<Form.Control as="select" value={name} onChange={e => setName(e.target.value)}>
				{
					data.map(element=> {
						return(<option key={element._id} value={element.category}>{element.category}</option>)
					})

				}
				</Form.Control>
			</Form.Group>
			<Form.Group controlId="type">
				<Form.Label>Category Type</Form.Label>
				<Form.Control as="select" value={type} onChange={e => setType(e.target.value)}>
					<option value="Income">Income</option>
					<option value="Expense">Expense</option>
				</Form.Control>
			</Form.Group>
			<Form.Group controlId="amount">
				<Form.Label>Amount</Form.Label>
				<Form.Control type="number" value={amount} onChange={e => setAmount(e.target.value)} required/>
			</Form.Group>
			<Form.Group controlId="total">
				<Form.Label>Current Balance</Form.Label>
				<Form.Control type="number" value={total} disabled/>
			</Form.Group>
			<Form.Group controlId="description">
				<Form.Label>Description</Form.Label>
				<Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required/>
			</Form.Group>
			{
				isActive ?
			<Button type="submit">Submit</Button>
			:
			<Button disabled>Submit</Button>
			}
			</Form>	
		</React.Fragment>


		)
}