import Router from 'next/router';
import {useEffect, useContext} from 'react';
import UserContext from '../../UserContext'

export default function Logout () {
	const {user, setUser} = useContext(UserContext)

	useEffect(() => {

		localStorage.clear()
		setUser({
			id : null
		})
		Router.push("/")
		
	},[])

	return null
}