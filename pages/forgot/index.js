import {useState, useEffect} from 'react';
import Router from 'next/router';
import Swal from 'sweetalert2';
import Head from 'next/head';
/*Bootstrap*/
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

export default function Forgot() {
	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [password2, setPassword2] = useState("")
	const [isActive, setIsActive] = useState("")

	useEffect(() => {
		if(email !==  "" && password !== "" && password2 !== "" && (password === password2)) {
			return setIsActive(true)
		} else {
			return setIsActive(false)
		}

	},[email, password, password2])

	const checkEmail = (e) => {
		e.preventDefault()
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}api/users/email-exist`, {
			method : "POST",
			headers : {
				"Content-Type" : "application/json"
			},

			body : JSON.stringify({
				email : email
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data) {
				fetch(`${process.env.NEXT_PUBLIC_BASE_URL}api/users/${email}`, {
					method : "PUT",
					headers : {
						"Content-Type" : "application/json"
					},

					body : JSON.stringify({
						email : email,
						password : password
					})
				})
				.then(res => res.json())
				.then(data => {
					if(data) {

						Swal.fire({
						  icon : "success",
						  text: 'Your password has been changed successfully!'
						})
						Router.push("/login")

					} else {
						Swal.fire({
						  icon : "error",
						  text: 'Something went wrong. Try again.'
						})
					}
				})

			} else {
				Swal.fire({
				  icon : "error",
				  text: 'Email does not exist'
				})
			}
		})
	}
	return(
			<React.Fragment>
			<Head>
			  <title>Forgot Password</title>
			  <meta charset="UTF-8" />
			  <meta name="description" content="Budget Tracker"/>
			  <meta name="keywords" content="HTML, CSS, JavaScript"/>
			  <meta name="viewport" content="initial-scale=1.0, width=device-width" />
			</Head>
			<Form onSubmit={e => checkEmail(e)}>
				<Form.Group controlId="email">
					<Form.Label>Enter Email</Form.Label>
					<Form.Control type="email" value={email} onChange={e => setEmail(e.target.value)}required/>
				</Form.Group>
				<Form.Group controlId="password">
					<Form.Label>Enter New Password</Form.Label>
					<Form.Control type="password" value={password} onChange={e => setPassword(e.target.value)}required/>
				</Form.Group>
				<Form.Group controlId="password2">
					<Form.Label>Confirm Password</Form.Label>
					<Form.Control type="password" value={password2} onChange={e => setPassword2(e.target.value)}required/>
				</Form.Group>		
				{
					isActive ?
					<Button type="submit">Submit</Button>
					:
					<Button disabled>Submit</Button>
				}
			</Form>
			</React.Fragment>

		)
}