import {Bar} from 'react-chartjs-2';


export default function BarChart ({figuresArray, label}) {

	const data = {

		labels : [

				'January',
				'February',
				'March',
				'April',
				'May',
				'June',
				'July',
				'August',
				'September',
				'October',
				'November',
				'December' 
			],

		datasets : [

				{
					label : label,
					backgroundColor : "indianred",
					borderColor : "black",
					borderWidth : 1,
					hoverBackgroundColor : "red",
					hoverBorderColor : "black",
					data : figuresArray


				}


			]
	}

	return(
			<Bar data={ data }/>

		)
}