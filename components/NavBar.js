import Link from 'next/link';
import {useContext} from 'react';
import UserContext from '../UserContext';
/*Bootstrap*/
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';



export default function NavBar () {

	const {user} = useContext(UserContext)

	return (

		<Navbar bg="light" expand="lg">
		  <Link href="/">
		  <a className="navbar-brand">Budget Tracker</a>
		  </Link>
		  <Navbar.Toggle aria-controls="basic-navbar-nav" />
		  <Navbar.Collapse id="basic-navbar-nav">
		    <Nav className="ml-auto">
		    {

		      user.id ? 
		      <React.Fragment>
		      <Link href="/">
		        <a className="nav-link">Home</a>
		      </Link>
		      <Link href="/categories">
		        <a className="nav-link">Categories</a>
		      </Link>
		      <Link href="/records">
		        <a className="nav-link">Records</a>
		      </Link>
		      <Link href="/chart">
		        <a className="nav-link">Insight</a>
		      </Link>
		      <Link href="/logout">
		        <a className="nav-link">Logout</a>
		      </Link>
		      </React.Fragment>
		      :
		      <React.Fragment>
		      <Link href="/login">
		        <a className="nav-link">Login</a>
		      </Link>
		      <Link href="/register">
		        <a className="nav-link">Register</a>
		      </Link>
		      </React.Fragment>
		    }
		    </Nav>
		  </Navbar.Collapse>
		</Navbar>

		)
}