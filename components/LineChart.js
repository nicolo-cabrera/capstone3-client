import {useEffect, useState} from 'react'
import {Line} from 'react-chartjs-2';

export default function LineChart ({figuresArray}) {

	const [lineData, setLineData]= useState([])
	useEffect(() => {

	setLineData(figuresArray.map(element => element.total))
	
	},[figuresArray])


	const data = {

		labels : lineData,
		datasets : [{

			label : "Total",
			data : lineData,
			backgroundColor: "rgba(75,192,192,0.2)",
			borderColor: "black"
		}]
	}
	return(
			<Line data={data}/>

		)
}
