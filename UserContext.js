import React from 'react';

/*Create a Context object*/
const UserContext = React.createContext()

/*Provider component that allows consumption of components and subscrib to context change*/

export const UserProvider = UserContext.Provider

export default UserContext

